angular.module('CuembyTest', ['ui.bootstrap.datetimepicker'])

.constant('Api', {
  url: 'http://localhost:8080/v1',
  uris: {
    assistance: '/assistance',
    user: '/user',
    position: '/position'
  },
  method: {
    get: 'GET',
    post: 'POST',
    patch: 'PATCH',
    delete: 'DELETE',
  }
})
.controller('Controller', ['$http', 'Api', '$filter', function ($http, Api, $filter) {
  
  const self = this;
  self.positions = [];
  self.user = {positionID: '', status: 'active'};
  
  
  self.save = (data) => {
    var positionID =  (self.selected) ? self.selected.id : null;
    var status = (self.selectedStatus) ? self.selectedStatus.id : null;
    
    var user = {
      identification: data.identification,
      name: data.name,
      lastName: data.lastName,
      positionID: positionID,
      birthday: $filter('date')(data.birthday, 'dd-MM-yyyy'),
      dateEntryCompany: $filter('date')(data.dateEntryCompany, 'dd-MM-yyyy'),
      status: data.status
    };
    
    var promise = $http({url: `${Api.url}${Api.uris.user}`, method: Api.method.post, data: user});
    promise.then(
      (success) => {
        alert(success.data.message);
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadPosition = () => {
    var promise = $http({url: `${Api.url}${Api.uris.position}`, method: Api.method.get});
    promise.then(
      (success) => {
        self.positions = success.data;
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadPosition();
}])

.controller('Update', ['$http', 'Api', function ($http, Api) {
  
  const self = this;
  self.users = [];
  self.user = {positionID: '', status: ''};
  self.status = [
    { name: 'No', id: 'active'},
    { name: 'Si', id: 'inactive'}
  ];
  
  
  self.save = (user) => {
    user.position = null;
    user.status = self.selectedStatus.id;
    var promise = $http({url: `${Api.url}${Api.uris.user}/${user.id}`, method: Api.method.patch, data: user});
    promise.then(
      (success) => {
        alert(success.data.message);
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadUsers = () => {
    var promise = $http({url: `${Api.url}${Api.uris.user}`, method: Api.method.get});
    promise.then(
      (success) => {
        self.users = success.data;
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadUsers();
}])

.controller('Report', ['$http', 'Api', '$filter', function ($http, Api, $filter) {
  
  const self = this;
  self.report = [];
  
  self.showReport = () => {
    
    var since = $filter('date')(self.since, 'dd-MM-yyyy');
    var until = $filter('date')(self.until, 'dd-MM-yyyy');
    
    var promise = $http({
      url: `${Api.url}${Api.uris.assistance}/?since=${since}&until=${until}`,
      method: Api.method.get
    });
    promise.then(
      (success) => {
        self.report = success.data;
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
}])
.controller('Assistance', ['$http', 'Api', '$filter', function ($http, Api, $filter) {
  
  const self = this;
  self.report = [];
  
  self.save = () => {
    
    var data = {
      userID: self.user.id,
      registerDate: $filter('date')(self.registerDate, 'dd-MM-yyyy')
    };
    
    var promise = $http({
      url: `${Api.url}${Api.uris.assistance}/`,
      method: Api.method.post,
      data: data
    });
    promise.then(
      (success) => {
        alert(success.data.message);
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadUsers = () => {
    var promise = $http({url: `${Api.url}${Api.uris.user}`, method: Api.method.get});
    promise.then(
      (success) => {
        self.users = success.data;
      },
      (error) => {
        alert(`Error ${error.status} ${error.data.message}`);
      }
    );
  };
  
  self.loadUsers();
  
}])
;
